import _NavBar from './NavBar.vue'

const NavBar = {
    install: function(Vue){
        Vue.component('NavBar', _NavBar)
    }
}

export default NavBar