import Vue from 'vue'
import Router from 'vue-router'
import Collect from '../components/Six/Collect'
import Classjump from '../components/Six/Classjump'
import Details from '../components/Six/Details'

import One from '@/components/One/One'
import Two from '@/components/Two/Two'
import Three from '@/components/Three/Three'
import Shopping from '@/components/Four/shopping/shopping'
import Payer from '@/components/Four/shopping/payer'
import Myorder from '@/components/Four/myorder/myorder'
import Myfootprint from '@/components/Four/myfootprint/myfootprint'
import Pay from '@/components/Four/myorder/pay'
import Five from '@/components/Five/Five'
import Main from '@/components/Main'
import Son from '../components/Five/Son'
import Detailss from '@/components/Two/Detailss/Detailss'
import Indexseek from "../components/One/Indexseek"
import Gond from "@/components/One/Onexo/Gond"
import Zhousi from "@/components/One/Onexo/Zhousi"
import Pingpa from "@/components/One/Onexo/Pingpa"


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/Classjump',
      name: 'Classjump',
      component: Classjump,
    },
    {
      path: '/Collect',
      name: 'Collect',
      component: Collect,
    },
    {
      path: '/Details',
      name: 'Details',
      component: Details,
    },
    // 我的订单  
    {
      path: '/fout/myorder',
      name: 'Myorder',
      component: Myorder,
    },
    //我的足迹
    {
      path: '/fout/myfootprint',
      name: 'Myfootprint',
      component: Myfootprint,
    },
    //是否支付
    {
      path: '/fout/myorder/pay',
      name: 'Pay',
      component: Pay
    },
    //去付款
    {
      path: '/fout/shopping/payer',
      name: 'Payer',
      component: Payer,
    },
    {
      path:'/onexo/gond',
      name:'Gond',
      component:Gond
    },
    {
      path:'/onexo/zhousi',
      name:'Zhousi',
      component:Zhousi
    },{
      path:'/onexo/pingpa',
      name:'Pingpa',
      component:Pingpa
    },
    {
      path: '/',
      name: 'main',
      component: Main,
      children: [
        {
          path: 'one',
          name: 'One',
          component: One
        },
        {
          path: 'two',
          name: 'Two',
          component: Two
        },
        {
          path: '/two/detailss',
          name: 'Detailss',
          component: Detailss
        },
        {
          path: 'three',
          name: 'Three',
          component: Three
        },
        {
          path: '/fout/shopping',
          name: 'Shopping',
          component: Shopping,
        },
        {
          path: 'five',
          name: 'Five',
          component: Five,
        }
      ]
    },
    {
      path: '/indexseek',
      name: 'Indexseek',
      component: Indexseek
    }
    ,
    {
    path: '/son',
          name: 'Son',
          component: Son
        }

  ]

})
