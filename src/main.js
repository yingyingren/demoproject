// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import 'reset-css';
import NavBar from '@/components/index.js'
import 'swiper/dist/css/swiper.css'

import {http} from '@/http'
Vue.prototype.$http=http


// import axios from 'axios'
// Vue.prototype.axios = axios; 
// Vue.prototype.baseURL = process.env.API_ROOT;



import Router from 'vue-router'
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

Vue.config.productionTip = false
Vue.use(NavBar)
import VueAwesomeSwiper from 'vue-awesome-swiper'
Vue.use(VueAwesomeSwiper)

import wcSwiper from 'wc-swiper'
import 'wc-swiper/style.css'
Vue.use(wcSwiper);

import axios from 'axios'
Vue.prototype.axios = axios;
Vue.prototype.baseURL = process.env.API_ROOT;

import LyTab from 'ly-tab'
Vue.use(LyTab)
import Mint from 'mint-ui';
Vue.use(Mint);

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

Vue.config.productionTip = false
// Vue.use(NavBar)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})