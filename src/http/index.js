import axios from 'axios'

const service = axios.create({
    baseURL: 'http://127.0.0.1:8360/api',
    headers: {
         "X-NideShop-Token":'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJpYXQiOjE1NzMxMDg2NjB9.oPBWeTb5sTHanJ_zySBXUunb_HXkD6zcgj1YNJFjLrk',
        "Authorization":'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJpYXQiOjE1NzMxMDg2NjB9.oPBWeTb5sTHanJ_zySBXUunb_HXkD6zcgj1YNJFjLrk'
    }
})
export function http(url, method, data, params) {
    return new Promise((resolve, reject) => {
        service({
            url,
            method,
            data,
            params
        })
        .then(res => {
            if(res.data.errno === 0){
                resolve(res.data)
            }else{
                reject(err)
            }
        })
    })
}

